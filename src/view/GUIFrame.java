package view;

import controller.FrameListener;
import controller.MonitorPanelListener;
import model.ModelImpl;
import model.Password;
import model.SampleListener;

import javax.swing.*;

import com.leapmotion.leap.Controller;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Created on 11/02/17.
 */
public class GUIFrame implements Observer{

    JFrame frame;
    JButton recordButton = new JButton("Set handshake");
    JButton monitorButton = new JButton("Compare");
    MonitorPanel monitorPanel;
    RecordPanel recordPanel;



    public GUIFrame(MonitorPanel mp, RecordPanel rp){
        populateFrame();

        monitorPanel = mp;
        recordPanel = rp;
    }

    private void populateFrame() {
        frame = new JFrame("Secret Handshake");

        JMenuBar menu = new JMenuBar();

        menu.add(recordButton);
        menu.add(monitorButton);
        frame.setJMenuBar(menu);

        //frame.setContentPane(container);

        frame.setMinimumSize(new Dimension(550,200));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void addController(ActionListener a){
        recordButton.addActionListener(a);
        monitorButton.addActionListener(a);
        System.out.println("GUIFrame.addController controller/listener added to frame");

    }

    public void monitorView(){
    	Password p = recordPanel.returnPassword();
    	monitorPanel.setPassword(p);
        System.out.println("GUIFrame.monitorView called");
        monitorButton.setEnabled(false);
        recordButton.setEnabled(true);
        frame.setContentPane(monitorPanel);
        frame.validate();
        frame.repaint();
        frame.pack();
        
    }

    public void recordView(){

        System.out.println("GUIFrame.recordView called");
        monitorButton.setEnabled(true);
        recordButton.setEnabled(false);
        frame.setContentPane(recordPanel);
        frame.validate();
        frame.repaint();
        frame.pack();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg == "record"){
            System.out.println("GUIFrame.update called (record)");

            recordView();

        }

        else if (arg == "monitor"){
            System.out.println("GUIFrame.update called (monitor)");

            monitorView();

        }
    }
}
