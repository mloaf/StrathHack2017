package view;

import controller.MonitorPanelListener;
import controllers.PasswordChecker;
import model.DataListener;
import model.HandCharacter;
import model.Password;

import javax.swing.*;

import com.leapmotion.leap.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Created on 11/02/17.
 */
public class MonitorPanel extends JPanel implements Observer {

    JLabel log = new JLabel("(0/0)"); //todo: get gesture series length to initialise this
    JButton listen;
    DataListener dl;
    Controller controller;
    Password original;

    public MonitorPanel(){

        populate();
    }

    private void populate(){
        JLabel l = new JLabel("Click 'listen' and perform the correct gesture series.");
        listen = new JButton("Listen");
        dl = new DataListener();
        controller = new Controller();
        controller.addListener(dl);
        
        listen.addActionListener(new ActionListener() 
        {
        	public void actionPerformed(ActionEvent e) {
        		System.out.println("Password is: " + original.toString());
        		printCorrectGestures(0, original.size());
        		for (int i = 0; i < original.size(); i++) {
        			HandCharacter originalHC = original.get(i);
        			HandCharacter thisHC = dl.capture();
        			HandCharacter[] originalHCarray = new HandCharacter[1];
        			HandCharacter[] thisHCarray = new HandCharacter[1];
        			originalHCarray[0] = originalHC;
        			thisHCarray[0] = thisHC;
        			
        			PasswordChecker pc = new PasswordChecker(originalHCarray, thisHCarray);
        			
        			// While the comparison between the original hand character and the current hand character is false, get a new hand character
        			while (!pc.checkHandCharacter(originalHC, thisHC)) {
        				thisHC = dl.capture();
        			}
        			System.out.println("CHARACTER " + i+1 + " MATCHED!!!");
        			printCorrectGestures(i+1, original.size());
        		}
        		
        		System.out.println("//////////////////////////////");
        		System.out.println("|Password Input Successfully|");
        		System.out.println("//////////////////////////////");
        		
        	}
        });

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx= 0;
        c.weighty= 0;
        c.ipadx = 10;
        c.ipady = 10;
        c.gridx = 0;
        c.gridy = 0;

        this.add(l,c);

        c.gridy = 1;

        this.add(listen, c);

        c.gridy = 2;

        this.add(log, c);
    }

    public void printCorrectGestures(int correct, int total){

        log.setText("("+correct+"/"+total+")");
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    public void addController(MonitorPanelListener m){
        listen.addActionListener(m);
    }
    
    public Password getPassword() {
    	return original;
    }
    
    public void setPassword(Password p) {
    	original = p;
    }
}