package view;

import controller.RecordPanelListener;
import model.DataListener;
import model.HandCharacter;
import model.Password;

import javax.swing.*;

import com.leapmotion.leap.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

/**
 * Created on 11/02/17.
 */
public class RecordPanel extends JPanel implements Observer{

    JButton capture;
    JButton reset;
    Controller controller;
    DataListener dl;
    HandCharacter hc;
    Password recorded;

    public RecordPanel(){

        populate();
    }

    private void populate(){
        JLabel l = new JLabel("Perform your gesture series. Click 'Capture' to add a gesture to the series.");
        capture = new JButton("Capture");
        reset = new JButton("Reset");
        controller = new Controller();
        dl = new DataListener();
        controller.addListener(dl);
        recorded = new Password();
        
        capture.addActionListener(new ActionListener() 
        {
        	public void actionPerformed(ActionEvent e) {
        		hc = dl.capture();
        		recorded.add(hc);
        		System.out.println(recorded.toString());		
        		dl.setPassword(recorded);
        	}
        });
        
        reset.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		recorded = new Password();
        	}
        });



        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx= 0;
        c.weighty= 0;
        c.ipadx = 10;
        c.ipady = 10;
        c.gridx = 0;
        c.gridy = 0;

        this.add(l,c);

        c.gridy = 1;

        this.add(capture, c);
        
        c.gridy = 3;
        this.add(reset, c);
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    public void addController(RecordPanelListener rpl){
        capture.addActionListener(rpl);
    }
    
    public Password returnPassword() {
    	return recorded;
    }
}
