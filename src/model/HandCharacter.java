package model;

import com.leapmotion.leap.Listener;
import com.leapmotion.leap.Vector;

public class HandCharacter {
	
	float pitch;
	float roll;
	Vector thumbPosition;
	Vector iFingerPosition;
	Vector mFingerPosition;
	Vector rFingerPosition;
	Vector pFingerPosition;
	Vector palmPosition;
	
	public HandCharacter(Vector palm, float p, float r, Vector thumb, Vector iFinger,
		Vector mFinger, Vector rFinger, Vector pfinger){
		palmPosition = palm;
		pitch = p;
		roll = r;
		thumbPosition = thumb;
		iFingerPosition = iFinger;
		mFingerPosition = mFinger;
		rFingerPosition = rFinger;
		pFingerPosition = pfinger;
	}
	
	
	public float getPitch(){
		return pitch;
	}
	
	public float getRoll(){
		return roll;
	}
	
	public Vector getPalmPosition(){
		return palmPosition;
	}
	
	void setPalmPosition(Vector p){
		palmPosition = p;
	}
	
	void setPitch(float p){
		pitch = p;
	}
	
	void setRoll(float r){
		roll = r;
	}
	
	void setThumbPosition(Vector t){
		thumbPosition = t;
	}
	
	void setIFingerPosition(Vector i){
		iFingerPosition = i;
	}
	
	void setMFingerPosition(Vector m){
		mFingerPosition = m;
	}
	
	void setRFingerPosition(Vector r){
		rFingerPosition = r;
	}
	
	void setPfingerPosition(Vector p){
		pFingerPosition = p;
	}
	
	public Vector getThumbPosition(){
		return thumbPosition;
	}
	
	public Vector getIFingerPosition(){
		return iFingerPosition;
	}
	
	public Vector getMFingerPosition(){
		return mFingerPosition;
	}
	
	public Vector getRFingerPosition(){
		return rFingerPosition;
	}
	
	public Vector getPFingerPosition(){
		return pFingerPosition;
	}
	
	
	@Override
	public String toString() {
		String s = "";
		s += ("Thumb: " + thumbPosition);
		s += ("\nIndex: " + iFingerPosition);
		s += ("\nMiddle: " + mFingerPosition);
		s += ("\nRing: " + rFingerPosition);
		s += ("\nPinky: " + pFingerPosition);
		s += ("\nRoll: " + roll);
		s+= ("\nPitch: " + pitch);
		
		return s;
	}
	
}