package model;

import java.util.ArrayList;

public class Password {

	private ArrayList<HandCharacter> password;
	
	public Password() {
		password = new ArrayList<HandCharacter>();
	}
	
	public void add(HandCharacter hc) {
		password.add(hc);
	}
	
	public HandCharacter get(int index) {
		return password.get(index);
	}
	
	public int size() {
		return password.size();
	}
	
	@Override
	public String toString() {
		String s = "";
		for (HandCharacter hc: password) {
			s += hc.toString();
		}
		
		return s;
	}
}
