package controller;

import model.Model;
import model.ModelImpl;
import view.MonitorPanel;
import view.RecordPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 11/02/17.
 */
public class MonitorPanelListener implements ActionListener {

    ModelImpl m;
    MonitorPanel mp;

    public MonitorPanelListener(){

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Set handshake": monitor();
                break;
        }
    }

    private void monitor(){

    }

    public void addModel(ModelImpl model){
        this.m = model;
    }

    public void addView(MonitorPanel monitorPanel){
        this.mp = monitorPanel;
    }
}
